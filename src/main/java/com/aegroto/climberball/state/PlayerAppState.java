/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aegroto.climberball.state;

import com.aegroto.climberball.chunk.TerrainChunk;
import com.aegroto.climberball.entity.EntityBall;
import com.aegroto.climberball.entity.pickup.EntityPickup;
import com.aegroto.climberball.menu.InGameMenu;
import com.aegroto.climberball.skin.Skin;
import com.aegroto.common.Coordinate2D;
import com.aegroto.common.Helpers;
import com.aegroto.gui.states.GuiAppState;
import com.jme3.app.Application;
import com.jme3.app.state.BaseAppState;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Quad;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author lorenzo
 */
public final class PlayerAppState extends BaseAppState implements ActionListener {
    private EntityBall ball;
    private final Skin skin;
    private final Node rootNode;
    protected LinkedList<TerrainChunk> chunkList;
    @Getter protected ArrayList<EntityPickup> pickupList;
    protected SoundAppState soundAppState;
    protected GuiAppState guiAppState;

    @Getter
    @Setter
    protected boolean gameLost = false;

    private Vector3f targetPos;
    //protected float xSpeed, ySpeed;

    @Getter
    protected int score;

    public PlayerAppState(Node rootNode,
            LinkedList<TerrainChunk> chunkList,
            ArrayList<EntityPickup> pickupList,
            GuiAppState guiAppState,
            Skin skin) {
        this.rootNode = rootNode;
        this.skin = skin;
        this.chunkList = chunkList;
        this.pickupList = pickupList;
        this.guiAppState = guiAppState;
    }

    @Override
    protected void initialize(Application app) {
        ball = new EntityBall(rootNode, app.getAssetManager(), skin);

        ball.setPos(new Vector2f(0f, chunkList.getFirst().getTargetVector().y));

        soundAppState = app.getStateManager().getState(SoundAppState.class);

        app.getInputManager().addMapping("Touch", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));

        app.getInputManager().addMapping("PlainForm", new KeyTrigger(KeyInput.KEY_S));
        app.getInputManager().addMapping("RockForm", new KeyTrigger(KeyInput.KEY_E));
        app.getInputManager().addMapping("SandForm", new KeyTrigger(KeyInput.KEY_F));
        app.getInputManager().addMapping("GrassForm", new KeyTrigger(KeyInput.KEY_R));

        app.getInputManager().addListener(this, "Touch", "PlainForm", "RockForm", "SandForm", "GrassForm");
    }

    @Override
    protected void cleanup(Application app) {
        ball.destroy();
    }

    @Override
    protected void onEnable() {
        ball.setXSpeed(Helpers.INITIAL_PLAYER_SPEED);
        ball.setYSpeed(Helpers.INITIAL_PLAYER_SPEED * .5f);

        //executor.execute(asynchronousTick);
    }

    @Override
    protected void onDisable() {

    }

    private boolean keepUpdating = true;
    private TerrainChunk lastChunk;

    @Override
    public void update(float tpf) {
        ball.update(tpf);
        
        if (!gameLost) {
            final TerrainChunk chunk;
            if(ball.getPos().x >= 0) 
                chunk = chunkList.get((int) (ball.getPos().x / Helpers.getTerrainChunkSize()));
            else 
                chunk = chunkList.getFirst();
            
            ball.setXSpeed(chunk.elaborateSpeedOnSurface(ball.getXSpeed(), ball.getCurrentForm()));

            float xSpeed = ball.getXSpeed() * tpf;
            float ySpeed = ball.getYSpeed() * tpf;

            ball.setRotationSpeed(xSpeed * .05f);

            targetPos = chunk.getTargetVector();

            float xAdd = ball.getPos().x > Helpers.getBallMaxX() ? 0 : xSpeed;
            float yAdd = FastMath.abs(ball.getPos().y - targetPos.y) > ySpeed
                    ? ball.getPos().y > targetPos.y ? - ySpeed : ySpeed
                    : 0;

            ball.safeSetPos(new Vector2f(
                    ball.getPos().x + xAdd - Helpers.INITIAL_PLAYER_SPEED * tpf,
                    ball.getPos().y + yAdd
            ));

            if (ball.getPos().x <= Helpers.getBallMinX()) {
                gameLost = true;
            }

            if (chunk != lastChunk) {
                score++;
                lastChunk = chunk;
            }
        }
    }

    public void useSecondChance() {
        gameLost = false;
        ball.safeSetPos(new Vector2f(0f, chunkList.getFirst().getTargetVector().y));
        
        ball.switchForm(EntityBall.FORM_PLAIN);
        
        ball.setXSpeed(Helpers.INITIAL_PLAYER_SPEED * 7.5f);
    }
    
    public void switchForm(byte form) {
        ball.switchForm(form);
        
        soundAppState.stopSound("effect_switch");
        soundAppState.playSound("effect_switch");
    }

    @Override
    public void onAction(String name, boolean isPressed, float tpf) {
        switch (name) {
            case "Touch":
                if (!isPressed && !gameLost) {
                    // boolean canSwitchForm = true;                    
                    Vector2f mousePos = getApplication().getInputManager().getCursorPosition();
                    
                    for(EntityPickup pickup:pickupList) {
                        if(Helpers.pointInArea(mousePos,
                                               pickup.getPickupZoneMin(),
                                               pickup.getPickupZoneMax()) &&
                            !pickup.isPicked()) {
                            guiAppState.getMenu(InGameMenu.class)
                                    .showInfoText("Whoa! You've got a " + pickup.getName() + " !");
                            pickup.applyOnBall(ball);
                            
                            // canSwitchForm = false;
                            break;
                        }
                    }
                }
                break;
            case "PlainForm":
                ball.switchForm(EntityBall.FORM_PLAIN);
                break;
            case "RockForm":
                ball.switchForm(EntityBall.FORM_ROCK);
                break;
            case "SandForm":
                ball.switchForm(EntityBall.FORM_SAND);
                break;
            case "GrassForm":
                ball.switchForm(EntityBall.FORM_GRASS);
                break;
        }
    }
}
