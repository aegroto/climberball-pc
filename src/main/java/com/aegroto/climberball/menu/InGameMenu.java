/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aegroto.climberball.menu;

import com.aegroto.climberball.entity.EntityBall;
import com.aegroto.climberball.skin.Skin;
import com.aegroto.climberball.state.PlayerAppState;
import com.aegroto.common.Coordinate2D;
import com.aegroto.common.Helpers;
import com.aegroto.gui.GUIButton;
import com.aegroto.gui.GUIImage;
import com.aegroto.gui.GUIMultipleLineText;
import com.aegroto.gui.GUIText;
import com.aegroto.gui.menu.Menu;
import com.aegroto.gui.states.GuiAppState;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.Vector2f;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import lombok.Getter;

/**
 *
 * @author lorenzo
 */
public final class InGameMenu extends Menu {
    private GUIText scoreText, infoText;
    
    private final Skin skin;
    
    public InGameMenu(Skin skin) {
        this.skin = skin;
    }

    @Override
    public void onAttach(GuiAppState guiAppState) {
        super.onAttach(guiAppState);
        
        if(scoreText == null) {
            scoreText=new GUIText(
                    new Coordinate2D(.05f,.95f).toVector(),
                    "Score: ", .3f,
                    guiAppState.getGuiFont(), 
                    guiAppState.getGuiConjunctionNode()
            );
        }
        
        scoreText.centerX();
        
        if(infoText == null) {
            infoText=new GUIText(
                    new Coordinate2D(0f,.85f).toVector(),
                    "", .3f,
                    guiAppState.getGuiFont(), 
                    guiAppState.getGuiConjunctionNode()
            );

            infoText.centerX();
        }
        
        Material switcherMat=new Material(guiAppState.getAssetManager(), "materials/FloatingSprite/FloatingSprite.j3md");
        switcherMat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);  
        
        attachElement(scoreText);
        attachElement(infoText);
    }
    
    public void showInfoText(String text) {
        infoText.setText(text);
        infoText.centerX();
        
        //final Timer timer = new Timer();
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                infoText.setText("");
            }
        }, 2000);
    }
 
    public void setScoreText(int score) {
        scoreText.setText("Score: "+score);
    }
}
