## Support

If you need any support about this boilerplate open an issue or send a mail at aegroto@protonmail.com

## License

Every content in this repository is released under MIT License: https://tldrlegal.com/license/mit-license